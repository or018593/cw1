# Example README.md showing required header and table format

| Module Code:                     |    CS1PC20                               |
|----------------------------------|-----------------------------------|
| Assignment report Title:         |        Coursework 1                           |
| Date (when the work completed):   |       1/12/23                           |
| Actual hrs spent for the assignment: |      15 hours                          |


## Analysis of effect of CoPilot on Pat's coding

| Impact                   | Positive         | Negative    |
|--------------------------|------------------------|-----------------------|
| on Learning                | Copilot assisted Pat in learning the basic framework needed to create a text-based game. Copilot came up with its own suggestions after Pat wrote down an idea, enhancing the flexibility of ideas that could be implemented into the game. Copilot also supplied the code for his user choice menu, aiding him in grasping the structure of how the optimal code is written. | Always relying on copilot may hinder Pat's problem thinking skills and impede his understanding of code - not allowing room for growth|
| on Productivity           | Copilot significantly boosted Pat's productivity as it generated code suggestions in real-time which accelerated the coding process - code written by copilot is also expected to be correct which saves more time as you do not have to debug much code. Pat is also now more familiar with specific tools i.e the GDP debugging program which saves time in the future as he doesn't have to learn it from scratch again. | Overreliance may hinder Pat's understanding of the code being implemented which may cause issues when troubleshooting. Copilots suggestions may not align with Pat's requirements, causing sub-optimal or incorrect code.                  |

## Analysis of effect of ChatGPT on my programming for tasks 3 and 4

| Impact                   | Positive         | Negative    |
|--------------------------|------------------------|-----------------------|
| on Learning                | ChatGPT has helped me with troubleshooting short code snippets and given me refined code that I can use in my projects in learning C - this has saved me loads of time when I struggled. ChatGPT also provides comments with explanations next to codes which makes it easy for me to understand what is happening. | I found that sometimes ChatGPT wouldn't give me the most optimal code and sometimes contained bugs, this is something I had to fix manually or come up with another prompt to improvise. |
| on Productivity           | ChatGPT helped me rewrite code to be more optimal and sorted out any unfunctional code to be made functional. It helped me with debugging which saved me so much time. | Dependence on ChatGPT may have hindered my problem-solving skills and some troubleshooting required me to spend more time as the code given by ChatGPT hadn't been understood fully by myself.              |

