    // Function to free allocated memory
void freeGame(game* game) {
    // Free memory for player
    free(game->player);

    // Free memory for each room
    for (int i = 0; i < game->numRooms; i++) {
        free(game->rooms[i].name);
        free(game->rooms[i].description);
        free(game->rooms[i].connections);
    }

    // Free memory for the array of rooms
    free(game->rooms);

    // Free memory for the game structure
    free(game);
}

// Function to play the game
void playGame() {
    // Create the game
    printf("Welcome to the game\n");
    game* game = createGame();

    // Print the initial room description
    printRoomDescription(game->player->currentRoom);

    // Game loop
    bool quit = false;
    while (!quit) {
        // Print the menu and get the user choice
        int choice = getMenuChoice();

        // Carry out the user choice
        if (choice == 1) {
            // Print the room description
            printRoomDescription(game->player->currentRoom);
        } else if (choice == 2) {
            // Get the user choice of room to move to
            int roomChoice = getRoomChoice(game->player->currentRoom);

            // Move the player to the chosen room
            movePlayer(game->player, roomChoice);
        } else if (choice == 3) {
            // Quit the game
            quit = true;
        }
    }

    // Free allocated memory when the game ends
    freeGame(game);
}

// Main function
int main() {
    // Play the game
    playGame();

    return 0;
}

The addition of the freeGame function ensures proper memory cleanup, preventing memory leaks and enhancing code robustness in adherence to best practices.
Imrpoved lines 211-237
